package general.projects.buhara_feature.data


import general.projects.buhara_feature.model.Student
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface UserApi {
    @GET("random_ten")
    suspend fun getAllAsync() : Deferred<List<Student>>
}