package general.projects.buhara_feature.di

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class Buhara : Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@Buhara)
            modules(listOf(viewModelModule , apiModel , netModule ,  repositoryModule))
        }
    }

}