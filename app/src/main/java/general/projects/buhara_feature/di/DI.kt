package general.projects.buhara_feature.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import general.projects.buhara_feature.data.UserApi
import general.projects.buhara_feature.repository.UserRepository
import general.projects.buhara_feature.viewmodel.UserViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.GlobalContext
import org.koin.core.context.GlobalContext.get
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {
    viewModel { UserViewModel(get()) }
}

val apiModel = module {
    fun getRequest(retrofit: Retrofit) : UserApi {
        return retrofit.create(UserApi::class.java)
    }
    single { getRequest(get()) }
}
val netModule = module {
    fun getRetrofit(factory : Gson) : Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://official-joke-api.appspot.com/")
            .addConverterFactory(GsonConverterFactory.create(factory))
            .build()
    }
    single { getRetrofit(get()) }
}
val repositoryModule = module {
    fun getUserRepository(api : UserApi) : UserRepository {
        return UserRepository(api)
    }
    single { getUserRepository(get()) }
}

