package general.projects.buhara_feature.viewmodel

data class LoadingState (val status : Status , val msg : String? = null){
companion object {
    val LOADED = LoadingState(Status.SUCCES)
    val LOADING = LoadingState(Status.RUNNING)
    fun error(msg : String?) = LoadingState(Status.FAILED , msg)
}
    enum class Status {
        RUNNING ,
        SUCCES ,
        FAILED
    }
}