package general.projects.buhara_feature.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import general.projects.buhara_feature.model.Student
import general.projects.buhara_feature.repository.UserRepository
import kotlinx.coroutines.launch
import java.lang.Exception

class UserViewModel(private val userRepository: UserRepository) : ViewModel() {
    val userList = MutableLiveData<List<Student>>()
    private val _loadingState = MutableLiveData<LoadingState>()
    val loadingState : LiveData<LoadingState>
    get() =  _loadingState
    init {
        viewModelScope.launch {
            userList.value = userRepository.refresh()
        }
        getData()
    }
    private fun getData(){
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                userRepository.refresh()
                _loadingState.value = LoadingState.LOADED
            }catch (e : Exception){
                _loadingState.value = LoadingState.error(e.message)
            }
        }
    }
}