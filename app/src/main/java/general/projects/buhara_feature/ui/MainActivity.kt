package general.projects.buhara_feature.ui
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import general.projects.buhara_feature.R
import general.projects.buhara_feature.databinding.ActivityMainBinding
import general.projects.buhara_feature.viewmodel.LoadingState
import general.projects.buhara_feature.viewmodel.UserViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {
    private val userViewModel by viewModel<UserViewModel>()
    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navController = findNavController(R.id.fragment)
        binding.bottomNavigationView.setupWithNavController(navController)
        //


        userViewModel.userList.observe(this , Observer {
        it.forEach { i ->
            Toast.makeText(baseContext , i.id.toString() , Toast.LENGTH_SHORT).show()
        }
        })
        userViewModel.loadingState.observe(this , Observer {
            when(it.status) {
                LoadingState.Status.FAILED -> Toast.makeText(this , it.msg , Toast.LENGTH_SHORT).show()
                LoadingState.Status.RUNNING -> Toast.makeText(this , "Идет загрузка" , Toast.LENGTH_SHORT).show()
                LoadingState.Status.SUCCES -> Toast.makeText(this , "Успешно" , Toast.LENGTH_SHORT).show()
            }
        })




    }
}