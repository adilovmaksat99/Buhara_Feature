package general.projects.buhara_feature.repository

import general.projects.buhara_feature.data.UserApi
import general.projects.buhara_feature.model.Student
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepository(private val userApi : UserApi) {
    private var _userList = listOf<Student>()
    suspend fun refresh() : List<Student> {
            _userList = userApi.getAllAsync().await()
        return _userList
    }
}